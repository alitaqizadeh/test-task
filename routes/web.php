<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Company routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'companies'
], function ($router) {

    // Comapany section include CRUD, company stations, list of companies and nearest station for a company
    Route::get('/', 'HomeController@companies');
    Route::match(array('GET', 'POST'), '/add', 'HomeController@companies_add');
    Route::match(array('GET', 'POST'), '/edit/{id}', 'HomeController@companies_edit');
    Route::get('/delete/{id}', 'HomeController@companies_delete');
    Route::get('/stations/{id}', 'HomeController@companies_stations');
    Route::match(array('GET', 'POST'), '/nearest-station','HomeController@nearest_station');
});

// Station routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'stations'
], function ($router) {

    // Station section include CRUD and list of companies
    Route::get('/', 'HomeController@stations');
    Route::match(array('GET', 'POST'), '/add', 'HomeController@stations_add');
    Route::match(array('GET', 'POST'), '/edit/{id}', 'HomeController@stations_edit');
    Route::get('/delete/{id}', 'HomeController@stations_delete');
});