<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'v1'
], function ($router) {

    // Token handling routes
    Route::post('register', 'TokenController@register');
    Route::post('login', 'TokenController@login');
    Route::post('me', 'TokenController@me');

    // Companies routes
    Route::resource('companies', 'CompanyController', [
        'only' => ['index', 'store', 'show', 'update', 'destroy']
    ]);
    Route::get('all-stations/{id}', 'CompanyController@all_stations');
    Route::post('nearest-stations', 'CompanyController@nearest_stations');

    // Stations routes
    Route::resource('stations', 'StationController', [
        'only' => ['index', 'store', 'show', 'update', 'destroy']
    ]);
});

