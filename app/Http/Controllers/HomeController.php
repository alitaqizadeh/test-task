<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * List all available companies
     */
    public function companies() {
        
        // Get token
        $token = $this->getToken();

        // Fetch list of all companies
        $url = url('http://127.0.0.1:8000/api/v1/companies');
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->asJson()
        ->get();

        return view('companies.index', compact('result'));
    }

    /**
     * Add new company
     */
    public function companies_add(Request $request) {
        if (! $_POST) {

            // Get token
            $token = $this->getToken();

            // Fetch list of all companies
            $url = url('http://127.0.0.1:8000/api/v1/companies');
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->asJson()
            ->get();

            return view('companies.add', compact('result'));
        }

        // Get token
        $token = $this->getToken();

        // Add company
        $url = url('http://127.0.0.1:8000/api/v1/companies/');
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->withData(array('parent_company_id' => $request->parent_company_id, 'name' => $request->name))
        ->asJson()
        ->post();
        
        Session::flash('status', json_encode($result));
        return redirect('companies');
    }

    /**
     * Company edit
     */
    public function companies_edit(Request $request, $id) {

        if (! $_POST) {

            // Get token
            $token = $this->getToken();

            // Fetch company informations
            $url = url('http://127.0.0.1:8000/api/v1/companies/'.$id);
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->asJson()
            ->get();

            return view('companies.edit', compact('result'));
        }

        // Get token
        $token = $this->getToken();

        // Update company data
        $url = url('http://127.0.0.1:8000/api/v1/companies/'.$id);
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->withData(array('parent_company_id' => $request->parent_company_id, 'name' => $request->name))
        ->patch();
        
        Session::flash('status', json_encode($result));
        return redirect('companies');
    }

    /**
     * Delete a company
     */
    public function companies_delete($id) {
        
        // Get token
        $token = $this->getToken();

        // Delete a company
        $url = url('http://127.0.0.1:8000/api/v1/companies/'.$id);
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->asJson()
        ->delete();

        Session::flash('status', json_encode($result));
        return redirect('companies');
    }

    /**
     * Fetch all stations for a company
     */
    public function companies_stations($id) {

        // Get token
        $token = $this->getToken();

        // Get all stations for a company
        $url = url('http://127.0.0.1:8000/api/v1/all-stations/'.$id);
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->asJson()
        ->get();

        return view('companies.stations', compact('result'));
    }

    /**
     * List all available stations
     */
    public function stations() {
        
        // Get token
        $token = $this->getToken();

        // Fetch list of all stations
        $url = url('http://127.0.0.1:8000/api/v1/stations');
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->asJson()
        ->get();

        return view('stations.index', compact('result'));
    }

    /**
     * Add new station
     */
    public function stations_add(Request $request) {
        if (! $_POST) {
            
            // Get token
            $token = $this->getToken();

            // Fetch list of all companies
            $url = url('http://127.0.0.1:8000/api/v1/companies');
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->asJson()
            ->get();

            return view('stations.add', compact('result'));
        }

        // Get token
        $token = $this->getToken();

        // Add station
        $url = url('http://127.0.0.1:8000/api/v1/stations/');
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->withData(array('name' => $request->name, 'latitude' => $request->latitude, 'longitude' => $request->longitude, 'company_id' => $request->company_id))
        ->asJson()
        ->post();
        
        Session::flash('status', json_encode($result));
        return redirect('stations');
    }

    /**
     * Station edit
     */
    public function stations_edit(Request $request, $id) {

        if (! $_POST) {

            // Get token
            $token = $this->getToken();

            // Fetch company informations
            $url = url('http://127.0.0.1:8000/api/v1/stations/'.$id);
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->asJson()
            ->get();

            return view('stations.edit', compact('result'));
        }

        // Get token
        $token = $this->getToken();

        // Update company data
        $url = url('http://127.0.0.1:8000/api/v1/stations/'.$id);
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->withData(array('name' => $request->name, 'latitude' => $request->latitude, 'longitude' => $request->longitude, 'company_id' => $request->company_id))
        ->patch();
        
        Session::flash('status', json_encode($result));
        return redirect('stations');
    }

    /**
     * Delete a station
     */
    public function stations_delete($id) {
        
        // Get token
        $token = $this->getToken();

        // Delete a station
        $url = url('http://127.0.0.1:8000/api/v1/stations/'.$id);
        $result = Curl::to($url)
        ->withHeader('Authorization: Bearer '.$token)
        ->asJson()
        ->delete();

        Session::flash('status', json_encode($result));
        return redirect('stations');
    }

    /**
     * Get nearest station for a company
     */
    public function nearest_station(Request $request) {

        if (! $_POST) {

            // Get token
            $token = $this->getToken();

            // Fetch company informations
            $url = url('http://127.0.0.1:8000/api/v1/companies/');
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->asJson()
            ->get();

            return view('nearest_station.index', compact('result'));
        }

            // Get token
            $token = $this->getToken();

            // Add station
            $url = url('http://127.0.0.1:8000/api/v1/nearest-stations/');
            $result = Curl::to($url)
            ->withHeader('Authorization: Bearer '.$token)
            ->withData(array('company_id' => $request->company_id, 'latitude' => $request->latitude, 'longitude' => $request->longitude, 'distance' => $request->distance))
            ->asJson()
            ->post();
            
            return view('nearest_station.results', compact('result'));
    }

    protected function getToken() {

        $url = url('http://127.0.0.1:8000/api/v1/login');

        $result = Curl::to($url)
        ->withData(array('email' => 'admin@admin.com', 'password' => 'admin'))
        ->asJson()
        ->post();

        return $result->token;
    }
}
