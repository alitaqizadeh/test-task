<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Company;
use App\Station;

class CompanyController extends Controller
{
    public function __construct() {

        $this->middleware('auth:api');
    }

    /**
     * List all companies
     */
    public function index() {

        // Using chunk for fetch huge amount of data and handling big data
        $companies = Company::all()->chunk(500);

        // Flat json array
        $companies = collect($companies);
        $companies = $companies->collapse();

        return $companies;
    }

    /**
     * Create new company
     */
    public function store(Request $request) {

        // Request validation
        $validator = Validator::make($request->only('parent_company_id', 'name'), [
            'parent_company_id' => 'required|numeric',
            'name' => 'required'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);
        
        // Create new company
        $company = new Company;
        $company->parent_company_id = $request->get('parent_company_id');
        $company->name = $request->get('name');
        $company->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'company registered successfully',
            'company_parent_id' => $company->parent_company_id,
            'name' => $company->name
        ], 200);
    }

    // Show company information
    public function show($id) {

        // Find company
        $company = Company::find($id);

        if (! $company)
            return response()->json([
                'status' => 'error',
                'message' => 'company not found',
            ], 404);

        return $company;
    }

    /**
     * Update a company
     */
    public function update(Request $request, $id) {
        
        // Find company
        $company = Company::find($id);

        if (! $company)
            return response()->json([
                'status' => 'error',
                'message' => 'company not found',
            ], 404);
        
        // Request validation
        $validator = Validator::make($request->only('parent_company_id', 'name'), [
            'parent_company_id' => 'numeric'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);
        
        // Check parent company exists or not
        if ($request->parent_company_id && ! $parent_company = Company::find($request->parent_company_id))
            return response()->json([
                'status' => 'error',
                'message' => 'parent company does not exists'
            ], 404);

        // Update company
        if ($request->parent_company_id)
            $company->parent_company_id = $request->parent_company_id;
        if ($request->name)
            $company->name = $request->name;
        if ($company->parent_company_id || $compay->name)
            $company->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'company updated successfully',
            'parent_company_id' => $company->parent_company_id,
            'name' => $company->name
        ], 200);
    }

    /**
     * Delete a company
     */
    public function destroy($id) {
        
        // Find company
        $company = Company::find($id);

        if (! $company)
            return response()->json([
                'status' => 'error',
                'message' => 'company not found'
            ], 404);
        
        $company->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'company deleted successfully'
        ]);
    }

    /**
     * Show all stations
     */
    public function all_stations($id) {

        // Get company tree
        $companies = $this->companies($id);

        // Get comapny stations
        $stations = $this->stations($companies);

        return $stations;
    }

    /**
     * Get company nearest station from a point 
     */
    public function nearest_stations(Request $request) {

        // Request validation
        $validator = Validator::make($request->only('company_id', 'latitude', 'longitude', 'distance'), [
            'company_id' => 'required|numeric',
            'latitude' => ['required', 'numeric', 'min:-99.99', 'max:99.99'],
            'longitude' => ['required', 'numeric', 'min:-999.99', 'max:999.99'],
            'distance' => 'numeric'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);
        
        $company_id = $request->company_id;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $distance = $request->distance;
        
        // Get company children
        $companies = $this->companies($company_id);

        if (! $companies)
            return response()->json([
                'status' => 'error',
                'message' => 'company not found'
            ], 400);

        foreach ($companies as $company)
            $companies_arr[] = $company->id;
        
        // Search for nearest stations for request location
        $nearest_stations = Station::select(DB::raw('*, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) as distance'))->having('distance', '<=', $distance)->whereIn('company_id', $companies_arr)->get();
        $nearest_stations = json_decode($nearest_stations, true);
        
        // Sort by nearest station
        usort($nearest_stations, function($x, $y) {
            return $x['distance'] <=> $y['distance'];
        });

        return $nearest_stations;
    }

    /**
     * Get all company children
     */
    protected function companies($id) {
        
        // Prepare nested function
        $companies = Company::all()->count();
        $companies = str_repeat('.companies', $companies - 1);
        $companies = 'companies' . $companies;

        // Get all company top children
        $companies = Company::with([$companies])->find($id);

        $ids = array();
        $data = json_decode($companies, true);

        array_walk_recursive($data, function($v, $k) use (&$ids) {
            if ($k === 'id') {
                $ids[] = $v;
            }
        });

        $ids = array_unique($ids);

        $companies = Company::whereIn('id', $ids)->get();

        return $companies;
    }

    /**
     * Get Company stations
     */
    protected function stations($companies) {

        // // Get all stations for specific company
        foreach ($companies as $company)
            $stations[] = Station::where('company_id', $company->id)->get();
        
        // Flat json array
        $stations = collect($stations);
        $stations = $stations->collapse();
        
        return $stations;
    }
}
