<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * Token handler class 
 */

class TokenController extends Controller
{
    public function __construct() {

        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Registe new user
     */
    public function register(Request $request) {

        // Request validation
        $validator = Validator::make($request->only('email', 'name', 'password'), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);
        
        // Create new user
        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->save();

        // Create token for new user
        $token = JWTAuth::fromUser($user);
        
        return response()->json([
            'status' => 'success',
            'token' => $token
        ], 200);
    }

    /**
     * Login
     */
    public function login(Request $request) {

        // Validation requests
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|string|email|max:255',
            'password' => 'required'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'invalid credentials'
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong while attempting to encode the token
            return response()->json([
                'status' => 'error',
                'message' => 'could not create token'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'token' => $token
        ], 200);
    }

    /**
     * User information
     */
    public function me() {

        return response()->json($this->guard()->user());
    }

    /**
     * Using gaurd during requests
     */
    public function guard() {

        return Auth::guard();
    }
}
