<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Station;
use App\Company;

class StationController extends Controller
{
    public function __construct() {

        // $this->middleware('auth:api');
    }

    /**
     * List all companies
     */
    public function index() {

        // Using chunk for fetch huge amount of data and handling big data
        $stations = Station::all()->chunk(500);

        $stations = $this->normalize($stations);

        return $stations;
    }

    /**
     * Create new station
     */
    public function store(Request $request) {

        // Request validation
        $validator = Validator::make($request->only('name', 'latitude', 'longitude', 'company_id'), [
            'name' => 'required',
            'latitude' => ['required', 'numeric', 'min:-99.99', 'max:99.99'],
            'longitude' => ['required', 'numeric', 'min:-999.99', 'max:999.99'],
            'company_id' => 'required|numeric'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);

        // Check parent company exists or not
        if (! $parent_company = Company::find($request->company_id))
            return response()->json([
                'status' => 'error',
                'message' => 'parent company does not exists'
            ], 404);
        
        // Create new station
        $station = new Station;
        $station->name = $request->get('name');
        $station->latitude = $request->get('latitude');
        $station->longitude = $request->get('longitude');
        $station->company_id = $request->get('company_id');
        $station->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'company registered successfully',
            'name' => $station->name,
            'latitude' => $station->latitude,
            'longitude' => $station->longitude,
            'company_id' => $station->company_id
        ], 200);
    }

    // Show station information
    public function show($id) {

        // Find station
        $station = Station::find($id);

        if (! $station)
            return response()->json([
                'status' => 'error',
                'message' => 'station not found',
            ], 404);

        return $station;
    }

    /**
     * Update a station
     */
    public function update(Request $request, $id) {
        
        // Find station
        $station = Station::find($id);

        if (! $station)
            return response()->json([
                'status' => 'error',
                'message' => 'station not found',
            ], 404);
        
        // Request validation
        $validator = Validator::make($request->only('name', 'latitude', 'longitude', 'company_id'), [
            'name' => 'required',
            'latitude' => ['required', 'numeric', 'min:-99.99', 'max:99.99'],
            'longitude' => ['required', 'numeric', 'min:-999.99', 'max:999.99'],
            'company_id' => 'required|numeric'
        ]);
        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors() 
            ], 400);
        
        // Check parent company exists or not
        if ($request->company_id && ! $parent_company = Station::find($request->company_id))
            return response()->json([
                'status' => 'error',
                'message' => 'parent company does not exists'
            ], 404);

        // Update station
        if ($request->name)
            $station->name = $request->name;
        if ($request->latitude)
            $station->latitude = $request->latitude;
        if ($request->longitude)
            $station->longitude = $request->longitude;
        if ($station->name || $station->latitude || $station->longitude || $station->company_id)
            $station->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'station updated successfully',
            'name' => $station->name,
            'latitude' => $station->latitude,
            'longitude' => $station->longitude,
            'compnay_id' => $station->company_id
        ], 200);
    }

    /**
     * Delete a station
     */
    public function destroy($id) {
        
        // Find station
        $station = Station::find($id);

        if (! $station)
            return response()->json([
                'status' => 'error',
                'message' => 'station not found'
            ], 404);
        
        $station->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'station deleted successfully'
        ]);
    }

    /**
     * For have a flat json you can convert chunked data by this function
     */
    protected function normalize($stations) {

        $result = [];
        foreach ($stations as $station) {
            foreach ($station as $s)
                $result[] = [
                    'id' => $s->id,
                    'name' => $s->name,
                    'name' => $s->name,
                    'latitude' => $s->latitude,
                    'longitude' => $s->longitude,
                    'company_id' => $s->company_id,
                    'created_at' => $s->created_at,
                    'updated_at' => $s-> updated_at,
                ];
        }

        return $result;
    }
}
