<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'latitude', 'longitude', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function companies() {

        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
