<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Company extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_company_id', 'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function companies() {

        return $this->hasMany('App\Company', 'parent_company_id', 'id')->whereRaw('companies.id != companies.parent_company_id');
    }

    public function stations() {
        
        return $this->hasMany('App\Station', 'company_id', 'id');
    }
}
