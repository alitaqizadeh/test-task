@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4><a href="companies"><span class="badge badge-warning" style="padding:16px">Companies</span></a></h4>
                    <br>
                    <h4><a href="stations"><span class="badge badge-warning" style="padding:16px">Stations</span></a></h4>
                    <br>
                    <h4><a href="companies/nearest-station"><span class="badge badge-warning" style="padding:16px">Get Nearest Station</span></a></h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
