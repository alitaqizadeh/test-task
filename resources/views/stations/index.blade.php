@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a href="stations/add" class="btn btn-sm btn-success">Add new Station</a>
                <div class="card-header">Stations</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row col-xs-12">
                        <div class="col-sm-1">ID</div>
                        <div class="col-sm-2">Name</div>
                        <div class="col-sm-2">Latitude</div>
                        <div class="col-sm-3">Longitude</div>
                        <div class="col-sm-2">CompnayID</div>
                        <div class="col-sm-2">Actions</div>
                    </div>
                    <hr>
                    @foreach($result as $r)
                    <div class="row col-sm-12">
                        <div class="col-sm-1">{{$r->id}}</div>
                        <div class="col-sm-2">{{$r->name}}</div>
                        <div class="col-sm-3">{{$r->latitude}}</div>
                        <div class="col-sm-3">{{$r->longitude}}</div>
                        <div class="col-sm-1">{{$r->company_id}}</div>
                        <div class="col-sm-2">
                            <a href="stations/edit/{{$r->id}}" type="button" class="btn btn-sm btn-info" style="font-size:10px">Edit</a>
                            <a href="stations/delete/{{$r->id}}" type="button" class="btn btn-sm btn-danger" style="font-size:10px">X</a>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection