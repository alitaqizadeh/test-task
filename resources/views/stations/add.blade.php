@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Station</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ Form::open(array('url' => 'stations/add/')) }}
                        <div class="form-group">
                            <label for="company_id" class="col-sm-6 control-label">Parent Company<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                                <select class="form-control" name="company_id">
                                    @foreach($result as $r)
                                        <option value="{{$r->id}}">{{$r->name}}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Station Name<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Company Name']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="latitude" class="col-sm-6 control-label">Latitude<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('latitude', '', ['class' => 'form-control', 'placeholder' => 'Latitude Ex. 58.399999']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="longitude" class="col-sm-6 control-label">Longitude<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('longitude', '', ['class' => 'form-control', 'placeholder' => 'Longitude Ex. 26.399999']) !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-2">
                            <button type="submit" class="form-control btn btn-success">Submit</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection