@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$result->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row col-xs-12">
                        <div class="col-sm-1">ID</div>
                        <div class="col-sm-2">Name</div>
                        <div class="col-sm-3">Latitude</div>
                        <div class="col-sm-3">Longitude</div>
                        <div class="col-sm-2">Company_id</div>
                    </div>
                    <hr>
                    <div class="row col-sm-12">
                        <div class="col-sm-1">{{$result->id}}</div>
                        <div class="col-sm-2">{{$result->name}}</div>
                        <div class="col-sm-3">{{$result->latitude}}</div>
                        <div class="col-sm-4">{{$result->longitude}}</div>
                        <div class="col-sm-1">{{$result->company_id}}</div>
                    </div>
                    <div class="card" style="margin-top:20px">
                        <div class="card-header">Edit {{$result->name}}</div>
                        <div class="card-body">
                            {{ Form::open(array('url' => 'stations/edit/'.$result->id)) }}
                                <div class="form-group">
                                    <label for="name" class="col-sm-6 control-label">Name<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('name', $result->name, ['class' => 'form-control', 'placeholder' => 'name']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="latitude" class="col-sm-6 control-label">Latitude<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('latitude', $result->latitude, ['class' => 'form-control', 'placeholder' => 'Latitude Ex. 58.399999']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="longitude" class="col-sm-6 control-label">Longitude<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('longitude', $result->latitude, ['class' => 'form-control', 'placeholder' => 'Longitude Ex. 26.399999']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="company_id" class="col-sm-6 control-label">Company ID<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('company_id', $result->company_id, ['class' => 'form-control', 'placeholder' => 'Company ID']) !!}
                                    </div>
                                </div>

                                <div class="form-group col-sm-2">
                                    <button type="submit" class="form-control btn btn-success">Submit</button>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection