@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Get Nearest Station</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ Form::open(array('url' => 'companies/nearest-station')) }}
                        <div class="form-group">
                            <label for="company_id" class="col-sm-6 control-label">Search In Which Company <em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                                <select class="form-control" name="company_id">
                                    @foreach($result as $r)
                                        <option value="{{$r->id}}">{{$r->name}}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="latitude" class="col-sm-6 control-label">Latitude<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('latitude', '', ['class' => 'form-control', 'placeholder' => 'Latitude Ex. 58.399999']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="longitude" class="col-sm-6 control-label">Longitude<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('longitude', '', ['class' => 'form-control', 'placeholder' => 'Longitude Ex. 26.399999']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="distance" class="col-sm-6 control-label">Distance(Radial Distance in KM)<em class="text-danger">*</em></label>

                            <div class="col-sm-6">
                            {!! Form::text('distance', '', ['class' => 'form-control', 'placeholder' => 'Distance EX. 30']) !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-2">
                            <button type="submit" class="form-control btn btn-success">Submit</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection