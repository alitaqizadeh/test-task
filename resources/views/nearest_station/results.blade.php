@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Stations ({{count($result)}} Stations)</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row col-xs-12">
                        <div class="col-sm-1">ID</div>
                        <div class="col-sm-2">Name</div>
                        <div class="col-sm-2">Latitude</div>
                        <div class="col-sm-2">Longitude</div>
                        <div class="col-sm-3">Distance</div>
                    </div>
                    <hr>
                    @foreach($result as $r)
                    <div class="row col-sm-12">
                        <div class="col-sm-1">{{$r->id}}</div>
                        <div class="col-sm-2">{{$r->name}}</div>
                        <div class="col-sm-2"style="font-size:10px">{{$r->latitude}}</div>
                        <div class="col-sm-2" style="font-size:10px">{{$r->longitude}}</div>
                        <div class="col-sm-3">{{$r->distance . ' KM'}}</div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection