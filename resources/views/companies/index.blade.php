@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a href="companies/add" class="btn btn-sm btn-success">Add new Comapny</a>
                <div class="card-header">Companies</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row col-xs-12">
                        <div class="col-sm-3">ID</div>
                        <div class="col-sm-3">Parent Company ID</div>
                        <div class="col-sm-3">Name</div>
                        <div class="col-sm-3">Actions</div>
                    </div>
                    <hr>
                    @foreach($result as $r)
                    <div class="row col-sm-12">
                        <div class="col-sm-3">{{$r->id}}</div>
                        <div class="col-sm-3">{{$r->parent_company_id}}</div>
                        <div class="col-sm-3">{{$r->name}}</div>
                        <div class="col-sm-3">
                            <a href="companies/stations/{{$r->id}}" type="button" class="btn btn-sm btn-success" style="font-size:10px">Stations</a>
                            <a href="companies/edit/{{$r->id}}" type="button" class="btn btn-sm btn-info" style="font-size:10px">Edit</a>
                            <a href="companies/delete/{{$r->id}}" type="button" class="btn btn-sm btn-danger" style="font-size:10px">X</a>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection