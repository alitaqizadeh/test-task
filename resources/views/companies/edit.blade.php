@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$result->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row col-xs-12">
                        <div class="col-sm-4">ID</div>
                        <div class="col-sm-4">Parent Company ID</div>
                        <div class="col-sm-4">Name</div>
                    </div>
                    <hr>
                    <div class="row col-sm-12">
                        <div class="col-sm-4">{{$result->id}}</div>
                        <div class="col-sm-4">{{$result->parent_company_id}}</div>
                        <div class="col-sm-4">{{$result->name}}</div>
                    </div>
                    <div class="card" style="margin-top:20px">
                        <div class="card-header">Edit {{$result->name}}</div>
                        <div class="card-body">
                            {{ Form::open(array('url' => 'companies/edit/'.$result->id)) }}
                                <div class="form-group">
                                    <label for="parent_company_id" class="col-sm-6 control-label">Parent Company ID<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('parent_company_id', $result->parent_company_id, ['class' => 'form-control', 'placeholder' => 'Parent Company ID']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-6 control-label">Company Name<em class="text-danger">*</em></label>

                                    <div class="col-sm-6">
                                    {!! Form::text('name', $result->name, ['class' => 'form-control', 'placeholder' => 'Company Name']) !!}
                                    </div>
                                </div>

                                <div class="form-group col-sm-2">
                                    <button type="submit" class="form-control btn btn-success">Submit</button>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection