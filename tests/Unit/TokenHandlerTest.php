<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TokenHandlerTest extends TestCase
{
    /**
     * A basic test for handling tokens
     *
     * @return void
     */
    public function testBasicTest()
    {
        // Login and get token test
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->json('POST', 'api/v1/login', ['email' => 'admin@admin.com', 'password' => 'admin']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'token' => true,
            ]);

        // Register and get token test
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->json('POST', 'api/v1/register', ['name' => 'ali', 'email' => 'ali@ali.com', 'password' => 'ali']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'token' => true,
            ]);
    }
}
