<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ixudra\Curl\Facades\Curl;

class StationTest extends TestCase
{
    /**
     * A basic test for station
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$this->getToken(),
        ])->json('GET', 'api/v1/stations');

        $response->assertStatus(200);

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$this->getToken(),
        ])->json('GET', 'api/v1/stations/1');

        $response->assertStatus(200);
    }

    protected function getToken() {

        $url = url('http://127.0.0.1:8000/api/v1/login');

        $result = Curl::to($url)
        ->withData(array('email' => 'admin@admin.com', 'password' => 'admin'))
        ->asJson()
        ->post();

        return $result->token;
    }
}
