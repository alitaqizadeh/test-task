<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define default companies
        $companies = [
            ['parent_company_id' => 1, 'name' => 'Company A'],
            ['parent_company_id' => 1, 'name' => 'Company B'],
            ['parent_company_id' => 2, 'name' => 'Company C'],
        ];

        // Insert to database
        DB::table('companies')->insert($companies);
    }
}
