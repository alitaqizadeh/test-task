<?php

use Illuminate\Database\Seeder;

class StationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define default companies
        $stations = [
            // Company A stations
            ['name' => 'station1', 'latitude' => '58.388321', 'longitude' => '26.752313', 'company_id' => '1'],
            ['name' => 'station2', 'latitude' => '58.391853', 'longitude' => '26.746761', 'company_id' => '1'],
            ['name' => 'station3', 'latitude' => '58.398961', 'longitude' => '26.724873', 'company_id' => '1'],
            ['name' => 'station4', 'latitude' => '58.405887', 'longitude' => '26.698779', 'company_id' => '1'],
            ['name' => 'station5', 'latitude' => '58.397066', 'longitude' => '26.616049', 'company_id' => '1'],
            ['name' => 'station6', 'latitude' => '58.369085', 'longitude' => '26.544322', 'company_id' => '1'],
            ['name' => 'station7', 'latitude' => '58.369573', 'longitude' => '26.351708', 'company_id' => '1'],
            ['name' => 'station8', 'latitude' => '58.400538', 'longitude' => '26.206130', 'company_id' => '1'],
            ['name' => 'station9', 'latitude' => '58.451587', 'longitude' => '25.944039', 'company_id' => '1'],
            ['name' => 'station10', 'latitude' => '58.547893', 'longitude' => '25.652810', 'company_id' => '1'],
            // Company B stations
            ['name' => 'station11', 'latitude' => '58.206499', 'longitude' => '25.132198', 'company_id' => '2'],
            ['name' => 'station12', 'latitude' => '56.933823', 'longitude' => '24.241862', 'company_id' => '2'],
            ['name' => 'station13', 'latitude' => '56.921598', 'longitude' => '24.266837', 'company_id' => '2'],
            ['name' => 'station14', 'latitude' => '56.800866', 'longitude' => '24.262770', 'company_id' => '2'],
            ['name' => 'station15', 'latitude' => '57.700024', 'longitude' => '24.406546', 'company_id' => '2'],
            // Company C stations
            ['name' => 'station16', 'latitude' => '57.839569', 'longitude' => '26.045579', 'company_id' => '3'],
            ['name' => 'station17', 'latitude' => '57.832740', 'longitude' => '26.592010', 'company_id' => '3'],
        ];

        // Insert to database
        DB::table('stations')->insert($stations);
    }
}
